#!/usr/bin/env perl

# (C) 2005, Artem Khodush <greenkaa@gmail.com>
# (C) 2020, Kyle J. McKay <mackyle@gmail.com>
# All rights reserved.

# This program contains parts from gitweb.cgi,
# (C) 2005, Kay Sievers <kay.sievers@vrfy.org>
# (C) 2005, Christian Gierke <ch@gierke.de>

# This program is licensed under the GPL v2, or a later version

use 5.008;
use strict;
no warnings;

package git::inner;

use Encode;
use File::Spec;
use IPC::Open2;
use vars qw($gitdir);

# location of the git-core binaries
$git::inner::gitbin="git";

# opens a "-|" cmd pipe handle with 2>/dev/null and returns it
sub cmd_pipe {
	open(NULL, '>', File::Spec->devnull) or die "Cannot open devnull: $!\n";
	open(SAVEERR, ">&STDERR") || die "couldn't dup STDERR: $!\n";
	open(STDERR, ">&NULL") || die "couldn't dup NULL to STDERR: $!\n";
	my $result = open(my $fd, "-|", @_);
	open(STDERR, ">&SAVEERR") || die "couldn't dup SAVERR to STDERR: $!\n";
	close(SAVEERR) or die "couldn't close SAVEERR: $!\n";
	close(NULL) or die "couldn't close NULL: $!\n";
	return $result ? $fd : undef;
}

# opens a "|-|" cmd pipe with 2>/dev/null and returns ($pid,$wfd,$rdf)
sub cmd_bidi_pipe {
	open(NULL, '>', File::Spec->devnull) or die "Cannot open devnull: $!\n";
	open(SAVEERR, ">&STDERR") || die "couldn't dup STDERR: $!\n";
	open(STDERR, ">&NULL") || die "couldn't dup NULL to STDERR: $!\n";
	my ($pid, $rfd, $wfd);
	eval {
		$pid = open2($rfd, $wfd, @_);
	};
	open(STDERR, ">&SAVEERR") || die "couldn't dup SAVERR to STDERR: $!\n";
	close(SAVEERR) or die "couldn't close SAVEERR: $!\n";
	close(NULL) or die "couldn't close NULL: $!\n";
	return $pid ? ($pid, $wfd, $rfd) : ();
}

# closes a cmd_bidi_pipe if passed the return array from cmd_bidi_pipe
sub cmd_bidi_close {
	my ($pid, $wfd, $rfd) = @_;
	defined($wfd) and close($wfd);
	defined($rfd) and close($rfd);
	$pid and waitpid($pid, 0);
}

# opens a "-|" git_cmd pipe handle with 2>/dev/null and returns it
# returns undef and sets a non-zero $! if the pipe is empty
sub git_cmd_pipe {
	my $p = cmd_pipe $git::inner::gitbin, "--git-dir=".$gitdir, @_;
	defined($p) or return undef;
	eof($p) or return $p;
	close($p);
	my $e = $?;
	$e = ($e >= 256) ? ($e > 32768 ? 128 : $e >> 8) : ($e & 0x7f) + 128;
	$e or $e = 1;
	$! = $e;
	return undef;
}

# opens a "-|" git_cmd pipe and returns the entire result as one chomp'd string
# or undef if there was no result or an error
sub git_cmd_value {
	my $p = git_cmd_pipe @_;
	defined($p) or return undef;
	my $result = undef;
	{
		local $/;
		$result = <$p>;
	}
	close($p);
	defined($result) and chomp($result);
	return $result;
}

my $fallback_encoding;
BEGIN {
	$fallback_encoding = Encode::find_encoding('Windows-1252');
	$fallback_encoding = Encode::find_encoding('ISO-8859-1')
		unless $fallback_encoding;
}

# decode sequences of octets in utf8 into Perl's internal form,
# which is utf-8 with utf8 flag set if needed.  git-browser writes out
# in utf-8 thanks to "binmode STDOUT, ':utf8'" at beginning
sub to_utf8 {
	my $str = shift || '';
	if (Encode::is_utf8($str) || utf8::decode($str)) {
		return $str;
	} else {
		return $fallback_encoding->decode($str, Encode::FB_DEFAULT);
	}
}

sub git_master_branch
{
	my $head_id = git_cmd_value 'rev-parse', '--verify', 'HEAD', '--';
	defined($head_id) && $head_id =~ /^[0-9a-f]{40,}$/ or return undef;
	my $head_sym = git_cmd_value 'symbolic-ref', '-q', 'HEAD';
	defined($head_sym) and $head_sym = to_utf8($head_sym);
	return "HEAD" unless defined($head_sym) && $head_sym =~ m{^refs/heads/(.+)$};
	return $1;
}

sub git_read_commits
{
	no strict 'refs';
	my $arg=shift;
	my $MAX_COUNT= $arg->{shortcomment} ? 400 : 200;
	my @command=('rev-list', '--stdin', '--header', '--parents', '--date-order', "--max-count=$MAX_COUNT", '--');
	my @input=(@{$arg->{id}}, map("^$_", @{$arg->{x}}));
	push(@input, '--');
	push(@input, @{$arg->{path}}) if @{$arg->{path}};

	my %commits;

	local $/ = "\0";
	my ($pid, $wfd, $rfd) = cmd_bidi_pipe($git::inner::gitbin, "--git-dir=".$gitdir, @command);
	$pid or die "git_read_commits: error running git rev-list: @{[0+$!]}\n";
	binmode $wfd;
	binmode $rfd;
	{
		local $SIG{'PIPE'} = sub {};
		print $wfd map("$_\n", @input)
			or cmd_bidi_close($pid, $wfd, $rfd), die "git_read_commits: git rev-list --stdin failed: $!\n";
	}
	close($wfd); $wfd = undef;
	while( my $commit_line=<$rfd> ) {
		$commit_line =~ s/\r$//;
		my @commit_lines = ();
		foreach (split '\n', $commit_line) {
			push @commit_lines, to_utf8($_);
		}
		pop @commit_lines;
		my %co;

		my $header = shift @commit_lines;
		if (!($header =~ m/^[0-9a-fA-F]{40}/)) {
			next;
		}
		($co{'id'}, my @parents) = split ' ', $header;
		$co{'parents'} = \@parents;
		while (my $line = shift @commit_lines) {
			last if $line eq "\n";
# minimize http traffic - do not read not used things
#			if ($line =~ m/^tree ([0-9a-fA-F]{40})$/) {
#				$co{'tree'} = $1;
#			} els
			if ($line =~ m/^author (.*) ([0-9]+) (.*)$/) {
				$co{'author'} = $1;
				$co{'author_epoch'} = $2;
#				$co{'author_tz'} = $3;
			}elsif ($line =~ m/^committer (.*) ([0-9]+) (.*)$/) {
#				$co{'committer'} = $1;
				$co{'committer_epoch'} = $2;
#				$co{'committer_tz'} = $3;
			}
		}
#		if (!defined $co{'tree'}) {
#			next;
#		};

		# remove added spaces
		foreach my $line (@commit_lines) {
			$line =~ s/^    //;
		}
		if( $arg->{shortcomment} ) {
			$co{'comment'} = [$commit_lines[0]];
		}else {
			$co{'comment'} = \@commit_lines;
		}

		$commits{$co{'id'}}=\%co;
	}
	cmd_bidi_close($pid, $wfd, $rfd);

	return \%commits;
}

sub get_ref_ids
{
	defined(my $fd = git_cmd_pipe 'show-ref', '--head', '--dereference') or
		die "get_ref_ids: error running git show-ref: @{[0+$!]}\n";
	my @refs;
	my %names;
	my $tagcnt = 0;
	while( my $line=<$fd> ) {
		chomp $line;
		my ($id,$name)=split /[ \t]+/, to_utf8($line);
		if( $name=~s/^refs\/heads\/// || $name eq "HEAD" ) {
			push @refs, { type=>"h", id=>$id, name=>$name };
		}elsif( $name=~s/^refs\/tags\/// ) {
			my $deref=0;
			if( $name=~m/\^\{\w*\}$/ ) { # it's dereferenced
				$deref=1;
				$name=$`;
			}
			# if several ids for a name is seen, we are interested only in the last dereferenced one
			++$tagcnt, $names{$name}={} unless exists $names{$name};
			$names{$name}->{$deref}=$id;
			push @refs, { type=>"t", id=>$id, name=>$name };
		}
	}
	close $fd;

	# keep only commits

	my ($pid, $wfd, $rfd);
	local $SIG{'PIPE'} = sub {};
	if ($tagcnt) {
		($pid, $wfd, $rfd) = cmd_bidi_pipe($git::inner::gitbin, "--git-dir=".$gitdir, 'cat-file', '--batch-check');
		$pid or die "get_ref_ids: unable to run git cat-file --batch-check\n";
	}
	my $git_get_type = sub {
		return "" unless $tagcnt;
		my $id = shift;
		defined($id) && $id ne "" or return "";
		print $wfd $id, "\n" or cmd_bidi_close($pid, $wfd, $rfd), die "get_ref_ids: git cat-file --batch-check failed: $!\n";
		my $bcl = <$rfd>;
		defined($bcl) or return "";
		my @bcl = split(' ', $bcl);
		@bcl >= 2 or return "";
		return $bcl[1];
	};
	my @result;
	for my $ref (@refs) {
		if( $ref->{type} eq "h" ) {
			# assume all heads are commits
			push @result, $ref;
		}else {
			my $id_kind=$names{$ref->{name}};
			# so. if several ids for a name is seen, keep only in the last dereferenced one
			if( $ref->{id} eq $id_kind->{1} || ($ref->{id} eq $id_kind->{0} && !exists $id_kind->{1}) ) {
				# and only if it's a commit
				push @result, $ref if &$git_get_type( $ref->{id} ) eq "commit";
			}
		}
	}
	cmd_bidi_close($pid, $wfd, $rfd) if $tagcnt;
	return \@result;
}

package git;

sub get_ref_names
{
	my $refs=git::inner::get_ref_ids;
	my $result={ tags=>[], heads=>[] };
	for my $ref (@$refs) {
		push @{$result->{tags}}, $ref->{name} if $ref->{type} eq "t";
		push @{$result->{heads}}, $ref->{name} if $ref->{type} eq "h";
	}
	return $result;
}

sub commits_from_refs
{
	my $arg=shift;
	# can't just do git_read_commits. mapping from ref names to ids must also be returned for labels to work.
	my $refs=git::inner::get_ref_ids;
	my @start_ids;
	for (@{$arg->{ref}}) {
		my ($type,$name)=split ",";
		$name = git::inner::to_utf8($name);
		if( "r" eq $type ) {
			push @start_ids, $_->{id} for (grep( $_->{type} =~ /^[ht]$/, @$refs )); # all heads & tags
		}else {
			push @start_ids, $_->{id} for (grep( $name eq $_->{name} && $type eq $_->{type}, @$refs ));
		}
	}
	my $master=git::inner::git_master_branch;
	my $ans = { refs=>$refs, commits=>commits_from_ids( { id=>\@start_ids, x=>$arg->{x}, path=>$arg->{path}, shortcomment=>$arg->{shortcomment} } ) };
	defined($master) && $master ne "" and $ans->{master} = $master;
	return $ans;
}

sub commits_from_ids
{
	my $arg=shift;
	return git::inner::git_read_commits( $arg );
}

package inner;

# Set the global doconfig setting in the GITBROWSER_CONFIG file to the full
# path to a perl source file to run to alter these settings

# If $check_path is set to a subroutine reference, it will be called
# by get_repo_path with two arguments, the name of the repo and its
# path which will be undef if it's not a known repo.  If the function
# returns false, access to the repo will be denied.
# $check_path = sub { my ($name, $path) = @_; $name ~! /restricted/i; };
use vars qw($check_path);

use Cwd qw(abs_path);
use File::Basename qw(dirname);
use File::Spec::Functions qw(file_name_is_absolute catdir);

sub read_config
{
	my $f;
	my $GITBROWSER_CONFIG = $ENV{'GITBROWSER_CONFIG'} || "git-browser.conf";
	-e $GITBROWSER_CONFIG or $GITBROWSER_CONFIG = "/etc/git-browser.conf";

	open $f, '<', $GITBROWSER_CONFIG or return;
	my $confdir = dirname(abs_path($GITBROWSER_CONFIG));
	my $section="";
	my $configfile="";
	while( <$f> ) {
		chomp;
		$_=~ s/\r$//;
		if( $section eq "repos" ) {
			if( m/^\w+:\s*/ ) {
				$section="";
				redo;
			}else {
				my ($name,$path)=split;
				if( $name && $path ) {
					file_name_is_absolute($path) or
						$path = catdir($confdir, $path);
					$inner::known_repos{$name}=$path;
				}
			}
		}else {
			if( m/^gitbin:\s*/ ) {
				$git::inner::gitbin=$';
			}elsif( m/^path:\s*/ ) {
				$ENV{PATH}=$';
			}elsif( m/^http_expires:\s*/ ) {
				$inner::http_expires=$';
			}elsif( m/^warehouse:\s*/ ) {
				my $path = $';
				file_name_is_absolute($path) or
					$path = catdir($confdir, $path);
				$inner::warehouse=$path;
			}elsif( m/^doconfig:\s*/ ) {
				$configfile=$';
			}elsif( m/^repos:\s*/ ) {
				$section="repos";
			}
		}
	}
	if ($configfile && -e $configfile) {
		do $configfile;
		die $@ if $@;
	}
}

package main;

use Cwd qw(abs_path);
use File::Basename qw(dirname);
use File::Spec::Functions qw(catdir);
use CGI qw(:standard :escapeHTML -nosticky);
use CGI::Util qw(unescape);
use CGI::Carp qw(fatalsToBrowser);
BEGIN {
	eval 'sub CGI::multi_param {CGI::param(@_)}'
		unless CGI->can("multi_param");
}
BEGIN {dirname(abs_path($0)) =~ m{^(.+)$}os and eval 'use lib $1'}
use JSON::Converter;

binmode STDOUT, ':utf8';

sub get_repo_path
{
	my ($name) = @_;
	my $path = $inner::known_repos{$name};
	return undef
	    if ref($inner::check_path) eq 'CODE' && !&{$inner::check_path}($name, $path);
	if (not defined $path and $inner::warehouse and -d catdir($inner::warehouse, $name)) {
		$path = catdir($inner::warehouse, $name);
	}
	return $path;
}

sub get_repo_names
{
	my @a=sort({lc($a) cmp lc($b) || $a cmp $b} keys %inner::known_repos);
	return \@a;
}
sub validate_input {
	my $input = shift;

	if ($input =~ m/^[0-9a-fA-F]{40,}$/) {
		return $input;
	}
	if ($input =~ m/(?:^|\/)(?:|\.|\.\.)(?:$|\/)/) {
		return undef;
	}
	if ($input =~ m/[^a-zA-Z0-9_\x80-\xff\ \t\.\/\-\+\*\~\%\,\x21-\x7e]/) {
		return undef;
	}
	return $input;
}

inner::read_config();

my $converter=JSON::Converter->new;
my $request=CGI::new();

my $repo;
my $sub;
my $arg={};

my $result="null";
my $error="null";

my @names=$request->multi_param;
for my $pn (@names) {
	if( $pn eq "repo" ) {
		$repo=$request->param( "repo" );
	}elsif( $pn eq "sub" ) {
		$sub=$request->param( "sub" );
	}else {
		my @v=$request->multi_param( $pn );
		for my $v (@v) {
			$error=$converter->valueToJson( "invalid cgi param value for '$pn': '$v'\n" ) unless defined validate_input( $v );
		}
		$arg->{$pn}=\@v;
	}
}

if( $error eq "null" ) {
	if( !defined( $sub ) ) {
		$error=$converter->valueToJson( "git-browser.cgi: 'sub' cgi parameter is omitted" );
	}elsif( exists $main::{$sub} ) {
		eval {
			$result=&{$main::{$sub}}( $arg );
		};
		if( $@ ) {
			$error=$converter->valueToJson( "error in main::$sub: $@" );
		}else {
			$result=$converter->objToJson( $result );
		}
	}elsif( exists $git::{$sub} ) {
		if( !defined( $repo ) ) {
			$error=$converter->valueToJson( "git-browser.cgi: 'repo' cgi parameter is omitted" );
		}elsif( !get_repo_path($repo) ) {
			$error=$converter->valueToJson( "git-browser.cgi: unknown repository name specified: $repo" );
		}else {
			local $git::inner::gitdir=get_repo_path($repo);
			eval {
				$result=&{$git::{$sub}}( $arg );
			};
			if( $@ ) {
				$error=$converter->valueToJson( "error in git::$sub: $@" );
			}else {
				$result=$converter->objToJson( $result );
			}
		}
	}else {
		$error=$converter->valueToJson( "git-browser.cgi: no procedure '$sub' in either git or main package" );
	}
}

print $request->header(-type=>'text/html',  -charset => 'utf-8', -status=> "200 OK", -expires => $inner::http_expires);

print <<EOF;
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript">
/* <![CDATA[ */
document.error=$error;
document.result=$result;
/* ]]> */
</script>
</head>
<body>
</body>
</html>
EOF

0;
