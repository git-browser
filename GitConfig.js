/* gitweb URL; gitweb must be in pathinfo mode */
cfg_gitweb_url="/cgi-bin/gitweb.cgi"

/* git-browser.cgi URL */
cfg_browsercgi_url="git-browser.cgi"

/* home button URL (default is "/") */
/* any "%n" is replaced with the name of the repo */
//cfg_home_url="/"

/* home button text (default is "home") */
//cfg_home_text="home"

/* alternate title for by-commit.html */
/* any %n is replaced with the name of the repo */
//cfg_bycommit_title="git browser"

/* alternate title for by-date.html */
/* any %n is replaced with the name of the repo */
//cfg_bydate_title="git browser"
