#!/usr/bin/env perl

# (C) 2005, Artem Khodush <greenkaa@gmail.com>
# (C) 2020, Kyle J. McKay <mackyle@gmail.com>
# All rights reserved.

# This program is licensed under the GPL v2, or a later version

use strict;
no warnings;

use CGI;

my $request=CGI::new();
my $sleep_seconds=$request->param( "sleep" );

sleep $sleep_seconds if $sleep_seconds;

print "Content-type: text/html; charset=utf-8\r\n\r\n";
print <<EOF;
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript">
/* <![CDATA[ */
document.result="ok";
/* ]]> */
</script>
</head>
<body></body>
</html>
EOF
