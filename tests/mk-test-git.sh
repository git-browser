#!/bin/sh

set -e
cd "$(dirname "$0")"

# git_add_config "some.var=value"
# every ' in value must be replaced with the 4-character sequence '\'' before
# calling this function or Git will barf.  Will not be effective unless running
# Git version 1.7.3 or later.
git_add_config() {
	GIT_CONFIG_PARAMETERS="${GIT_CONFIG_PARAMETERS:+$GIT_CONFIG_PARAMETERS }'$1'"
	export GIT_CONFIG_PARAMETERS
}
git_add_config "user.name=Git-Browser Test"
git_add_config "user.email=-"
git_add_config "rerere.enabled=false"

mkdir test.git
export GIT_DIR=test.git
git init
git config core.bare false
git config core.logallrefupdates false

echo "Hello World" >hello
echo "Silly example" >example
mkdir dir
echo "file in a dir" >dir/file

git add hello example dir/file
echo "It's a new day for git" >>hello
git commit -m "initial commit"
sleep 1

git add hello
git commit -m "bring hello up to date"
sleep 1
git tag my-first-tag

git checkout -b mybranch
echo "Work, work, work" >>hello
git commit -m 'Some work.' hello
sleep 1


git checkout master
echo "Play, play, play" >>hello
echo "Lots of fun" >>example
git commit -m 'Some fun.' hello example
sleep 1


git merge -m "Merge work in mybranch" mybranch || :
echo "Hello World" >hello
echo "It's a new day for git" >>hello
echo "Play, play, play" >>hello
echo "Work, work, work" >>hello
git add hello
git commit -m "mybranch merged"

rm hello
rm example
rm dir/file
rmdir dir
git config core.bare true
rm -f "$GIT_DIR/index" "$GIT_DIR/ORIG_HEAD" "$GIT_DIR/COMMIT_EDITMSG"
git pack-refs --all --prune
git repack -a -d
git prune --expire=now


cat <<CONF >../git-browser.conf
gitbin: $(command -v git)
warehouse: $(cd .. && pwd -P)
repos:
test tests/test.git
boo no-such-dir
CONF

cat <<'CONF' >../lighttpd.conf
port = 8080
include_shell "echo \"confdir = \\"`pwd -P`\\"\""
server.document-root = confdir
server.port = port
server.bind = "127.0.0.1"
$SERVER["socket"] == "[::1]:"+port {}
server.modules = ( "mod_alias", "mod_cgi", "mod_accesslog" )
server.indexfiles = ( "index.html" )
cgi.assign = ( ".cgi" => "" )
mimetype.assign = ( ".html" => "text/html", ".css" => "text/css", ".js" => "text/javascript" )
alias.url = ( "/git-browser/" => confdir + "/" )
accesslog.filename = "/dev/stderr"
CONF
